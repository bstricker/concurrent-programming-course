
/**
 * Result class that stores benchmark results.
 *
 * @author Gruppe 6: Schmid, Stricker, Laqua
 */
public class Result {

    private final double avgExecTime;
    private final int operationsPerSecond;

    public Result(int operationsPerSecond, double avgExecTime) {
        this.operationsPerSecond = operationsPerSecond;
        this.avgExecTime = avgExecTime;
    }

    public int getOperationsPerSecond() {
        return operationsPerSecond;
    }


    public double getAvgExecTime() {
        return avgExecTime;
    }




    public String printResult() {


        return "operationsPerSecond/s: " + operationsPerSecond + " Avg Time per Run: " + getAvgExecTime();

    }

    @Override
    public String toString() {
        return printResult();
    }


}
