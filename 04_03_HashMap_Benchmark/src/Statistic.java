import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Calculates a bar chart comparing the different results.
 * <p/>
 *
 * @author Gruppe 6: Schmid, Stricker, Laqua
 */
public class Statistic {

    private Map<Integer, Result> concResults = new TreeMap<Integer, Result>();
    private Map<Integer, Result> syncResults = new TreeMap<Integer, Result>();
    private Map<Integer, Result> unsyncResults = new TreeMap<Integer, Result>();

    private int maxOperationsConc;
    private int maxOperationsSync;
    private int maxOperationsUnsync;

    private double maxTimeConc;
    private double maxTimeSync;
    private double maxTimeUnsync;


    public void addConcurrentResults(Result concResult, int threads) {

        this.concResults.put(threads, concResult);


        int maxOperationPerSec = concResult.getOperationsPerSecond();
        double maxTime = concResult.getAvgExecTime();


        if (maxOperationPerSec > maxOperationsConc) {
            maxOperationsConc = maxOperationPerSec;
        }

        if (maxTime > maxTimeConc) {
            maxTimeConc = maxTime;
        }


    }


    public void addSynchronizedResults(Result syncResult, int threads) {
        this.syncResults.put(threads, syncResult);


        int maxOperationPerSec = syncResult.getOperationsPerSecond();
        double maxTime = syncResult.getAvgExecTime();

        if (maxOperationPerSec > maxOperationsSync) {
            maxOperationsSync = maxOperationPerSec;
        }

        if (maxTime > maxTimeSync) {
            maxTimeSync = maxTime;
        }
    }

    public void addUnsynchronizedResults(Result unsyncResult, int threads) {
        this.unsyncResults.put(threads, unsyncResult);


        int maxOperationPerSec = unsyncResult.getOperationsPerSecond();
        double maxTime = unsyncResult.getAvgExecTime();

        if (maxOperationPerSec > maxOperationsUnsync) {
            maxOperationsUnsync = maxOperationPerSec;
        }


        if (maxTime > maxTimeUnsync) {
            maxTimeUnsync = maxTime;
        }

    }


    public void printStatistic() {


        printConcurrent();

        printSynchronized();

        printUnsynchronized();

    }


    private void printUnsynchronized() {
        System.out.println("---------------------------------------");
        System.out.println("Unsynchronized (normal) Hashmap");
        System.out.println("---------------------------------------");


        Set<Map.Entry<Integer, Result>> results = unsyncResults.entrySet();

        for (Map.Entry<Integer, Result> r : results) {

            System.out.println("Threads:\t" + r.getKey());

            float opPerSecondPercentage = r.getValue().getOperationsPerSecond() / (float) maxOperationsUnsync;
            double timePercentage = r.getValue().getAvgExecTime() / maxTimeUnsync;

            printOperationsPerSecond(r, opPerSecondPercentage);

            printAvgTime(r, timePercentage);
            System.out.println();


        }
    }

    private void printSynchronized() {
        System.out.println("---------------------------------------");
        System.out.println("Synchronized Hashmap");
        System.out.println("---------------------------------------");


        Set<Map.Entry<Integer, Result>> results = syncResults.entrySet();

        for (Map.Entry<Integer, Result> r : results) {

            System.out.println("Threads:\t" + r.getKey());

            float opPerSecondPercentage = r.getValue().getOperationsPerSecond() / (float) maxOperationsSync;
            double timePercentage = r.getValue().getAvgExecTime() / maxTimeSync;

            printOperationsPerSecond(r, opPerSecondPercentage);
            printAvgTime(r, timePercentage);


        }
    }

    private void printConcurrent() {
        System.out.println("---------------------------------------");
        System.out.println("Concurrent Hashmap");
        System.out.println("---------------------------------------");


        Set<Map.Entry<Integer, Result>> results = concResults.entrySet();


        for (Map.Entry<Integer, Result> r : results) {

            System.out.println("Threads:\t" + r.getKey());

            float opPerSecondPercentage = r.getValue().getOperationsPerSecond() / (float) maxOperationsConc;
            double timePercentage = r.getValue().getAvgExecTime() / maxTimeConc;

            printOperationsPerSecond(r, opPerSecondPercentage);

            printAvgTime(r, timePercentage);


        }
    }


    private void printAvgTime(Map.Entry<Integer, Result> r, double timePercentage) {
        System.out.printf("Elapsed Time:\t\t\t%10.4f\t", r.getValue().getAvgExecTime());

        for (int i = 0, max = (int) Math.round(timePercentage * 50); i < max; i++) {

            System.out.print("-");

        }
        System.out.println();
    }

    private void printOperationsPerSecond(Map.Entry<Integer, Result> r, float operationsPerSecondPercentage) {
        System.out.printf("Operations per Second:\t%,10d\t", r.getValue().getOperationsPerSecond());


        for (int i = 0, max = Math.round(operationsPerSecondPercentage * 50); i < max; i++) {

            System.out.print("-");

        }
        System.out.println();
    }

    public void addResult(Result result, int threads, Map<Integer, Integer> map) {

        if (map instanceof ConcurrentHashMap) {
            addConcurrentResults(result, threads);
        } else if (map instanceof SynchronizedHashMap) {
            addSynchronizedResults(result, threads);
        } else if (map instanceof HashMap) {
            addUnsynchronizedResults(result, threads);
        } else {
            throw new IllegalStateException("Map is no HashMap");
        }

    }
}
