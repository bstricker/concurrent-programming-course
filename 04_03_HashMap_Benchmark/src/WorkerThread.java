import java.util.Map;
import java.util.Random;
import java.util.concurrent.Callable;

/**
 * Worker Thread that executes the benchmark for a map.
 * <p/>
 * Configuration is made in the Constructor of the WorkerThread.
 * On each operation the Thread decides randomized with operation (insert, read, remove) should be executed.
 * The probability of each one is specified in the Constructor.
 *
 * @author Gruppe 6: Schmid, Stricker, Laqua
 */
public class WorkerThread implements Callable<Result> {

    private final Map map;

    private Random rand = new Random();

    private final int runs;
    private final int operations;


    private int PercInserts;
    private int PerReads;
    private int PerRemoves;

    private int operation;

    /**
     * Creates a new WorkerThread Object
     * <p/>
     * Configuration of benchmark process is set here.
     * The second (percInserts) to fourth (percRemoves) are the probability of this operation in percent.
     * <p/>
     * E.g.: 50 % Inserts, 25 % Reads, 25 % Removes => 50, 25, 25
     * <p/>
     * NOTE: The sum of all three parameters has to be 100.
     *
     * @param map         the map to operate on
     * @param percInserts Probability of a insert operation in percent (e.g. 50 for 50 %)
     * @param percReads   Probability of a read (get) operation in percent (e.g. 25 for 25 %)
     * @param percRemoves Probability of a remove operation in percent (e.g. 25 for 25 %)
     * @param runs        the number of runs to execute the benchmark
     * @param operations  the number of operations
     * @throws IllegalArgumentException if the sum of the three operation probability is unequal 100
     *                                  or if the map is null
     */
    public WorkerThread(Map map, int percInserts, int percReads, int percRemoves, int runs, int operations) {

        if (percInserts + percReads + percRemoves != 100) {
            throw new IllegalArgumentException("Sum isn't 100 Percent " + percInserts + " " + percReads + " " + percRemoves);
        }
        if (map == null) {
            throw new IllegalArgumentException("Map not initialized");
        }

        this.map = map;
        this.runs = runs;
        this.operations = operations;

        this.PercInserts = percInserts;
        this.PerReads = percReads;
        this.PerRemoves = percRemoves;


    }


    @Override
    public Result call() {


        long startTime, endTime, avgExecTime;
        double avgExecTimeSec;
        int operationsPerSecond;


        startTime = System.nanoTime();

        for (int run = 0; run < runs; run++) {

            // Loading Screen
            System.out.print(".");

            for (int amountOfOperations = 0; amountOfOperations < operations; amountOfOperations++) {


                execOperation();

            }


        }


        endTime = System.nanoTime();

        // calc result per run
        avgExecTime = (endTime - startTime) / runs;
        operation /= runs;

        // concert measured time in nanoseconds to seconds
        avgExecTimeSec = avgExecTime / Math.pow(10, 9);

        // operations per seconds
        operationsPerSecond = (int) Math.round(operation / avgExecTimeSec);


        return new Result(operationsPerSecond, avgExecTimeSec);
    }

    /**
     * Executes map operation (put, read(containsValue), remove) on the basis of the generated number.
     *
     * @throws IllegalStateException if the sum of the probabilities of the operations is unequal 100.
     */
    private void execOperation() {

        int number = rand.nextInt(100);


        if (number < PercInserts) {

            map.put(number, number);
            operation++;

        } else if (number < PercInserts + PerReads) {

            map.containsValue(number);
            operation++;

        } else if (number < PercInserts + PerReads + PerRemoves) {

            map.remove(number);
            operation++;

        } else {
            throw new IllegalStateException("Random number '" + number + "' >= Sum of PercInserts, PerReads and PerRemoves");
        }
    }
}
