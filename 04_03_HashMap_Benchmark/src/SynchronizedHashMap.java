import java.util.HashMap;

/**
 * Own Implementation of a SynchronizedHashMap.
 *
 * NOTE: This Class is NOT thread safe!
 * Only the Methods put, get and remove are synchronized.
 *
 *
 * @author Gruppe 6: Schmid, Stricker, Laqua
 */
public class SynchronizedHashMap<K, V> extends HashMap<K, V> {

    @Override
    public synchronized V put(K key, V value) {
        return super.put(key, value);    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    public synchronized V get(Object key) {
        return super.get(key);    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    public synchronized V remove(Object key) {
        return super.remove(key);    //To change body of overridden methods use File | Settings | File Templates.
    }
}
