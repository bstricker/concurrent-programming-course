import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Main Test who is responsible for the WorkerThreads.
 * <p/>
 * Run a Hashmap benchmark with three different HashMaps.
 * - ConcurrentHashMap  thread safe map from java's concurrent package
 * - SynchronizedHashMap Own implementation of a thread safe map
 * - HashMap    unsynchronized map of javas util package
 *
 * @author Gruppe 6: Schmid, Stricker, Laqua
 */
public class MainTest {

    private static final int N_THREADS = 16;
    private static final int RUNS = 5;
    private static final int OPERATIONS = 1000000;

    private static final int PERC_INSERTS = 50;
    private static final int PERC_READS = 25;
    private static final int PERC_REMOVES = 25;

    private static List<Map<Integer, Integer>> maps = new LinkedList<Map<Integer, Integer>>();

    private static Statistic statistic = new Statistic();

    private static void initMaps() {

        maps.add(new ConcurrentHashMap<Integer, Integer>());
        maps.add(new HashMap<Integer, Integer>());
        maps.add(new SynchronizedHashMap<Integer, Integer>());


    }


    public static void main(String[] args) throws InterruptedException, ExecutionException {

        initMaps();

        ThreadPoolExecutor exec = new ThreadPoolExecutor(N_THREADS, N_THREADS, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
        List<Callable<Result>> callables = new LinkedList<Callable<Result>>();
        List<Future<Result>> futureResults;

        System.out.println("Runs:\t" + RUNS);

        // loop for the different maps
        for (Map<Integer, Integer> map : maps) {

            System.out.println(map.getClass().getSimpleName());

            // loop for the different amount of parallel threads
            for (int threads = 1; threads <= N_THREADS; threads *= 2) {

                System.out.println("-------------------------------------");
                System.out.println("Start " + threads + " threads");

                // loop for adding all current threads
                for (int i = 0; i < threads; i++) {
                    callables.add(new WorkerThread(map, PERC_INSERTS, PERC_READS, PERC_REMOVES, RUNS, OPERATIONS));
                }


                // start all callables
                futureResults = exec.invokeAll(callables);


                List<Result> results = getListOfResults(futureResults);

                System.out.println();
                System.out.println("All " + threads + " Threads finished");

                // calc average result of parallel threads
                Result result = calcAvgResult(results);


                statistic.addResult(result, threads, map);

                callables.clear();
                futureResults.clear();

            }

            System.out.println();

        }
        exec.shutdown();

        exec.awaitTermination(30, TimeUnit.SECONDS);

        statistic.printStatistic();

    }

    /**
     * Calculates the average benchmark values (Operation per Second, Execution Time) of the executing threads.
     *
     * @param results results to calculate the average
     * @return the calculated average
     */
    private static Result calcAvgResult(List<Result> results) {

        int opsPerSecond = 0;
        double avgExecTime = 0.0;
        int threads = results.size();

        for (Result r : results) {

            opsPerSecond += r.getOperationsPerSecond();
            avgExecTime += r.getAvgExecTime();
        }

        opsPerSecond /= threads;
        avgExecTime /= threads;

        return new Result(opsPerSecond, avgExecTime);
    }

    /**
     * Convert a List of Futures holding the Result into a List of Results
     *
     * @param futureResults List of Futures containing the Results
     * @return converted List of the results
     */
    private static List<Result> getListOfResults(List<Future<Result>> futureResults) {

        List<Result> results = new LinkedList<Result>();

        for (Future<Result> futureResult : futureResults) {
            try {
                results.add(futureResult.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }


        return results;
    }

}
