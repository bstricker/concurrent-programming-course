import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Main Thread
 * <p/>
 * Runs the Monte-Carlo Simulation with different Executor Services.
 * <p/>
 * Number of Threads (nThreads) is the double of CPUs available;
 * <p/>
 * Used Executors:
 * - SingleThreadExecutor
 * - CachedThreadPool (default values)
 * - FixedThreadPool
 * - CachedThreadPool (nThread sized Pool and unbounded Queue)
 */
public class MCSMaster {

    private static final boolean USE_VISUAL_VM = true;
    private static final int NUM_RUNNABLES = 2000;
    private static final int m = 2000000;
    private static final double s0 = 100.0;
    private static final double t = 1.0;
    private static final double r = 0.05;
    private static final double sig = 0.2;

    private static final double exprT = Math.exp(-r * t);

    // Display result with two decimals
    private static final DecimalFormat df = new DecimalFormat("#.##");

    public static void main(String[] args) throws InterruptedException, ExecutionException {

        if (USE_VISUAL_VM)
            // sleep to give VisualVM enough time to start
            Thread.sleep(5000);

        int cpus = Runtime.getRuntime().availableProcessors();
        int nThreads = cpus * 2;

        int n = 100;
        int iterations = m / NUM_RUNNABLES;

        double dt;
        double nuDt;
        double sigSqrtDt;

        long startTime;
        long endTime;
        double execTimeInS;

        // different executor services
        List<ExecutorService> executorServices = new ArrayList<ExecutorService>();
        executorServices.add(Executors.unconfigurableExecutorService(Executors.newSingleThreadExecutor()));
        executorServices.add(Executors.unconfigurableExecutorService(Executors.newCachedThreadPool()));
        executorServices.add(Executors.unconfigurableExecutorService(Executors.newFixedThreadPool(nThreads)));

        executorServices.add(Executors.unconfigurableExecutorService(
                new ThreadPoolExecutor(
                        nThreads,
                        nThreads,
                        500,        // time before idle thread will be terminated
                        TimeUnit.MILLISECONDS,
                        new LinkedBlockingDeque<Runnable>())));

        dt = t / (double) n;
        nuDt = (r - 0.5 * sig * sig) * dt;
        sigSqrtDt = sig * Math.sqrt(dt);


        List<Future<Double>> futureResults;

        double result = 0.0;

        List<Callable<Double>> callables = new ArrayList<Callable<Double>>(nThreads);

        // create all Workers
        for (int i = 0; i < NUM_RUNNABLES; i++) {

            callables.add(new MCSWorker(iterations, s0, nuDt, sigSqrtDt));
        }

        // Run Simulation with every executor service
        for (ExecutorService exec : executorServices) {

            System.out.println("Start " + exec.toString());
            System.out.println("Workers:\t" + nThreads);
            System.out.println("Iterations (all):\t" + m);
            System.out.println("Iterations (Worker):\t" + iterations);

            startTime = System.nanoTime();
            try {
                futureResults = exec.invokeAll(callables);

                result = getResultFromFutures(futureResults);
            } catch (RejectedExecutionException e) {
                e.printStackTrace();
            }
            endTime = System.nanoTime();


            System.out.println("Finished " + exec.toString());
            exec.shutdown();
            exec.awaitTermination(5, TimeUnit.SECONDS);

            // Execution time in Seconds
            execTimeInS = (endTime - startTime) / Math.pow(10, 9);

            System.out.println("Result: " + result);
            System.out.println("Execution time: " + df.format(execTimeInS) + " seconds\n");

        }

    }

    private static double getResultFromFutures(List<Future<Double>> futureResults) throws ExecutionException, InterruptedException {

        double sum = 0.0;

        for (Future<Double> future : futureResults) {
            sum += future.get();
        }


        return sum * exprT / (double) m;
    }

}
