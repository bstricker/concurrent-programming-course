// Monte Carlo simulation of a European call option

import java.util.Random;
import java.util.concurrent.Callable;

//-------------------------------
public class MCSWorker implements Callable<Double> {
//-------------------------------

    private final double s0;
    private final int m;
    private final double sigSqrtDt;
    private final double nuDt;


    public MCSWorker(int m, double s0, double nuDt, double sigSqrtDt) {

        this.m = m;
        this.s0 = s0;
        this.nuDt = nuDt;
        this.sigSqrtDt = sigSqrtDt;

    }


    @Override
    public Double call() throws Exception {

        Random random = new Random();



        double k = 100.0;
        int n = 100;

        double sum = 0.0;
        double eps, ct, st;

        for (int j = 1; j <= m; j++) {
            st = s0;
            for (int i = 1; i <= n; i++) {
                eps = random.nextGaussian();
                st *= Math.exp(nuDt + sigSqrtDt * eps);
            }
            ct = Math.max(0.0, st - k);
            sum += ct;

        }



        return sum;
    }
}
