import java.text.DecimalFormat;
import java.util.Queue;
import java.util.concurrent.*;

/**
 * Main Thread
 * <p/>
 * Runs the Monte-Carlo Simulation in a ThreadPool and uses a Executor policy.
 * <p/>
 * Creates MCSWorkers and a RejectedThreadHandler.
 * Uses a ThreadPoolExecutor to handle the amount of MCSWorkers.
 * ExecutorService uses the AbortPolicy to reject Workers if the ThreadPool is full.
 * This rejected threads will be passed to the RejectedThreadHandler that retry to submit this threads.
 * <p/>
 * After creating every Thread, the MainThread collects the results from the Futures till the RejectedThreadHandler is finished and all threads are executed.
 */
public class MCSMasterReject {

    private static final boolean USE_VISUAL_VM = true;
    private static final int NUM_RUNNABLES = 2000;
    private static final int m = 2000000;
    private static final double s0 = 100.0;
    private static final double t = 1.0;
    private static final double r = 0.05;
    private static final double sig = 0.2;

    private static final double exprT = Math.exp(-r * t);

    // Display result with two decimals
    private static final DecimalFormat df = new DecimalFormat("#.##");


    public static void main(String[] args) throws InterruptedException {

        if (USE_VISUAL_VM)
            // sleep to give VisualVM enough time to start
            Thread.sleep(5000);

        int cpus = Runtime.getRuntime().availableProcessors();
        int nThreads = cpus * 2;

        int n = 100;
        int iterations = m / NUM_RUNNABLES;

        double dt;
        double nuDt;
        double sigSqrtDt;

        long startTime;
        long endTime;
        double execTimeInS;

        /*
         * Put Futures in this queue. Main Thread and RejectedThreadHandler will be accessing this Queue, so it is synchronized.
         */
        Queue<Future<Double>> futureResults = new LinkedBlockingQueue<Future<Double>>();


        ExecutorService executorService = Executors.unconfigurableExecutorService(
                new ThreadPoolExecutor(
                        nThreads,
                        nThreads,
                        500,        // time before idle thread will be terminated
                        TimeUnit.MILLISECONDS,
                        new ArrayBlockingQueue<Runnable>(nThreads),
                        new ThreadPoolExecutor.AbortPolicy()));


        // Thread that handles rejected threads
        RejectedThreadHandler rejectedThreadHandler = new RejectedThreadHandler(executorService, futureResults);
        rejectedThreadHandler.start();

        dt = t / (double) n;
        nuDt = (r - 0.5 * sig * sig) * dt;
        sigSqrtDt = sig * Math.sqrt(dt);


        double result;

        // Initializing
        for (int i = 0; i < NUM_RUNNABLES; i++) {

            MCSWorker thread = new MCSWorker(iterations, s0, nuDt, sigSqrtDt);

            try {
                synchronized (futureResults) {
                    futureResults.add(executorService.submit(thread));
                }
            } catch (RejectedExecutionException e) {
                // Thread has been rejected, so put it in the handler queue
                rejectedThreadHandler.add(thread);
            }

        }


        System.out.println("Start " + executorService.toString());
        System.out.println("Workers:\t" + nThreads);
        System.out.println("Iterations (all):\t" + m);
        System.out.println("Iterations (Worker):\t" + iterations);


        startTime = System.nanoTime();


        // Interrupt handle to tell him that no more Threads will be added
        rejectedThreadHandler.noMoreThreadsSubmitting();

        Double sum = 0.0;
        /*
         * While handler is alive poll Results from the queue and calc result.
         * Handler will be still adding threads to the queue.
          */

        while (rejectedThreadHandler.isAlive()) {
            try {
                synchronized (futureResults) {
                    Future<Double> future = futureResults.poll();

                    if (future != null) {
                        sum += future.get();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        result = sum * exprT / (double) m;

        endTime = System.nanoTime();

        System.out.println("Finished " + executorService.toString());

        // All threads are finished
        executorService.shutdown();
        executorService.awaitTermination(5, TimeUnit.SECONDS);


        // Execution time in Seconds
        execTimeInS = (endTime - startTime) / Math.pow(10, 9);


        System.out.println("Result: " + result);
        System.out.println("Execution time: " + df.format(execTimeInS) + " seconds\n");


    }


}
