import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;

/**
 * Handles Rejected Threads.
 * Stores rejected threads in a Queue and permanently submit these to the executor service. Retry as long as all threads are successfully submitted.
 */
public class RejectedThreadHandler extends Thread {

    private final Queue<Future<Double>> doneThreads;
    private final ExecutorService executorService;
    private final Queue<MCSWorker> rejectedThreads = new LinkedBlockingQueue<MCSWorker>();

    public RejectedThreadHandler(ExecutorService executorService, Queue<Future<Double>> doneThreads) {
        this.setName("RejectedThreadHandler");
        this.executorService = executorService;
        this.doneThreads = doneThreads;
    }

    @Override
    public void run() {


        while (!rejectedThreads.isEmpty() || !Thread.currentThread().isInterrupted()) {

            MCSWorker rejectedThread = remove();
            System.out.println(rejectedThreads.size());

            if (rejectedThread != null) {

                try {

                    synchronized (doneThreads) {
                        doneThreads.add(executorService.submit(rejectedThread));
                    }

                } catch (RejectedExecutionException e) {
                    add(rejectedThread);
                }

            }
        }
    }

    /**
     * Add a rejected Thread to the queue.
     * This thread will be tried to submit it to the ExecutorService as long as it has been submitted successfully.
     *
     * @param rejectedThread Rejected Thread that will be tried to resubmitting to ExecutorService
     */
    public synchronized void add(MCSWorker rejectedThread) {
        rejectedThreads.add(rejectedThread);
    }

    /**
     * Get first rejected Thread.
     *
     * @return Head of the rejected thread queue
     */
    private synchronized MCSWorker remove() {
        return rejectedThreads.poll();
    }

    /**
     * Tell handler that no more threads will be created and submitted to the ExecutorService.
     */
    public void noMoreThreadsSubmitting() {

        System.out.println("Cancel Handler");
        interrupt();

    }
}
