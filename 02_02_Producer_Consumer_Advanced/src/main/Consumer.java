package main;

import java.util.LinkedList;
import java.util.List;

/**
 * Consumer class
 * <p/>
 * Thread reads elements from buffer periodically or sleep if buffer is empty.
 *
 * @author Gruppe 6: Schmid, Stricker, Laqua
 */
public class Consumer implements Runnable {


    private String name;


    private Controller controller;


    /**
     * Creates a new Consumer
     */
    public Consumer(String name, Controller controller) {

        this.name = name;
        this.controller = controller;

    }


    @Override
    public void run() {

        List<Integer> numbers = new LinkedList<Integer>();


        do {

            numbers.clear();

            numbers = controller.tryToConsume(this);


            if (!numbers.isEmpty()) {
                System.out.println("\t\t\t\t\t\t" + name + ":\tRead " + numbers);
            }


        } while (!numbers.contains(0));


        System.out
                .println("\t\t\t\t\t\t" + name + ":\tRead Zero. Terminating.");

    }
}
