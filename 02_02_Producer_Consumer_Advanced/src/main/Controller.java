package main;

import java.util.*;

public class Controller {


    // Map to every consumer (at the moment only first and second possible) a list of producers from which they must consume numbers
    private Map<Consumer, List<Producer>> consumerProducers = new HashMap<Consumer, List<Producer>>();

    // all producers
    private List<Producer> producers = new LinkedList<Producer>();

    public Controller() {

    }


    public void addProducer(Producer producer) {

        this.producers.add(producer);

    }


    /**
     * Add first Consumer to the Controller.
     * This consumer will get all producers to consume from.
     *
     * @param consumer the first consumer
     */
    public void addFirstConsumer(Consumer consumer) {

        // add all producers to consumer
        consumerProducers.put(consumer, producers);
    }


    /**
     * Add second Consumer to the Controller.
     * This consumer will get every second producer to consume from.
     *
     * @param consumer the second consumer
     */
    public void addSecondConsumer(Consumer consumer) {

        List<Producer> respProducers = new LinkedList<Producer>();

        int i = 0;
        for (Producer p : producers) {
            if (i % 2 == 0) {
                respProducers.add(p);
            }
            i++;
        }

        consumerProducers.put(consumer, respProducers);

    }


    /**
     * Trying to consume from the producers that are responsible for the specified consumer.
     * <p/>
     * First check if all responsible producers have something to consume (no empty buffer).
     * If all producers are ready the list of Integer will be build and returned.
     * If not every producer is ready, an empty list will returned.
     *
     * @param consumer the Consumer which tries to consume from his producers
     * @return the consumed Numbers, or an empty list if not all are ready
     */
    public synchronized List<Integer> tryToConsume(Consumer consumer) {

        List<Producer> producers = consumerProducers.get(consumer);

        // numbers we get from producer
        List<Integer> numbers = new ArrayList<Integer>();


        if (areAllConsumable(producers)) {

            List<Producer> producersToRemove = new ArrayList<Producer>();

            for (Producer p : producers) {

                int number = p.getBuffer().getNextElement();

                numbers.add(number);

                if (number == 0) {
                    producersToRemove.add(p);
                }

            }

            for (Producer producerToRemove : producersToRemove) {


                producers.remove(producerToRemove);

                for (Map.Entry<Consumer, List<Producer>> entry : consumerProducers.entrySet()) {
                    entry.getValue().remove(producerToRemove);
                }

                producerToRemove.stop();

            }


        } else {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return numbers;


    }

    /**
     * Check if all specified producers are consumable.
     *
     * @param producers List of producers which can be consumable or not
     * @return true if all are consumable, false otherwise
     */
    private boolean areAllConsumable(List<Producer> producers) {

        for (Producer p : producers) {

            if (!p.isConsumable()) {
                return false;
            }
        }

        return true;

    }

    public synchronized void wakeUpConsumer() {

        this.notifyAll();


    }
}
