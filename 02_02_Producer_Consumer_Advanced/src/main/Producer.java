
package main;

import java.util.Random;

/**
 * Producer class
 * <p/>
 * Thread writes elements into buffer periodically and sleeps if buffer is
 * already full.
 *
 * @author Gruppe 6: Schmid, Stricker, Laqua
 */
public class Producer implements Runnable {

    private static final int MAX_NUMBER = 20;

    private Buffer buffer;
    private Controller controller;
    private int minSleepTime;
    private int maxSleepTime;
    private int fullBufferSleepTime;


    private boolean run = true;
    private String name;



    /**
     * Creates a new Producer
     *
     * @param buffer              the buffer to write into
     * @param minSleepTime        minimal time to sleep after writing into buffer
     * @param maxSleepTime        maximal time to sleep after writing into buffer
     * @param fullBufferSleepTime time to sleep if buffer is full
     */
    public Producer(String name, Buffer buffer, Controller controller, int minSleepTime, int maxSleepTime,
                    int fullBufferSleepTime) {

        this.name = name;
        this.buffer = buffer;
        this.controller = controller;
        this.minSleepTime = minSleepTime;
        this.maxSleepTime = maxSleepTime;
        this.fullBufferSleepTime = fullBufferSleepTime;

    }

    public boolean isConsumable() {
        return !buffer.isBufferEmpty();
    }

    public Buffer getBuffer() {
        return buffer;
    }


    public void stop() {
        this.run = false;
    }

    @Override
    public void run() {

        Random generator = new Random();
        int number;
        int sleepTime;


        do {

            if (buffer.isBufferFull()) {

                System.out.println(name + ": Full main.Buffer. Sleep for "
                        + fullBufferSleepTime + " milliseconds.");

                try {
                    Thread.sleep(fullBufferSleepTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            } else {

                // Random number from 0 to MAX_NUMBER
                number = generator.nextInt(MAX_NUMBER + 1);

                if (number == 0) {
                    System.out.println(name + ": Generated " + number + "!!!!!!!!!!!");
                } else {
                    System.out.println(name + ": Generated " + number);
                }
                buffer.addElement(number);

                // wake all waiting Consumers, to let them check again if all buffers are ready
               controller.wakeUpConsumer();

                sleepTime = generator.nextInt(maxSleepTime - minSleepTime);
                sleepTime += minSleepTime;

                try {
                    Thread.sleep(sleepTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

        } while (run);

        System.out.println(name + ": Forced to terminate.");
    }


}
