package main;

import java.util.ArrayList;
import java.util.List;

/**
 * Main class
 * <p/>
 * Creates Buffer, creates and starts Producer and Consumer and waits until
 * everybody has terminated.
 *
 * @author Gruppe 6: Schmid, Stricker, Laqua
 */
public class MainTest {

    private static final int MIN_TIME = 0;
    private static final int MAX_TIME = 3000;
    private static final int FULL_TIME = 1000;

    public static void main(String[] args) {

        Controller controller = new Controller();


        Buffer buffer1 = new Buffer(10);
        Buffer buffer2 = new Buffer(10);
        Buffer buffer3 = new Buffer(10);
        Buffer buffer4 = new Buffer(10);

        List<Producer> producers = new ArrayList<Producer>();

        Producer producer1 = new Producer("Producer 1", buffer1, controller, MIN_TIME, MAX_TIME, FULL_TIME);
        Producer producer2 = new Producer("Producer 2", buffer2, controller, MIN_TIME, MAX_TIME, FULL_TIME);
        Producer producer3 = new Producer("Producer 3", buffer3, controller, MIN_TIME, MAX_TIME, FULL_TIME);
        Producer producer4 = new Producer("Producer 4", buffer4, controller, MIN_TIME, MAX_TIME, FULL_TIME);

        producers.add(producer1);
        producers.add(producer2);
        producers.add(producer3);
        producers.add(producer4);


        Consumer consumer1 = new Consumer("Consumer 1", controller);
        Consumer consumer2 = new Consumer("Consumer 2", controller);

        controller.addProducer(producer1);
        controller.addProducer(producer2);
        controller.addProducer(producer3);
        controller.addProducer(producer4);

        controller.addFirstConsumer(consumer1);
        controller.addSecondConsumer(consumer2);


        Thread producerThread1 = new Thread(producer1, "p1");
        Thread producerThread2 = new Thread(producer2, "p2");
        Thread producerThread3 = new Thread(producer3, "p3");
        Thread producerThread4 = new Thread(producer4, "p4");

        Thread consumerThread1 = new Thread(consumer1, "c1");
        Thread consumerThread2 = new Thread(consumer2, "c2");

        producerThread1.start();
        producerThread2.start();
        producerThread3.start();
        producerThread4.start();

        consumerThread1.start();
        consumerThread2.start();


        try {
            consumerThread1.join();
            consumerThread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (Producer p : producers) {
            p.stop();
        }

        try {
            producerThread1.join();
            producerThread2.join();
            producerThread3.join();
            producerThread4.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
