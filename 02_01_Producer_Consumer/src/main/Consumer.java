package main;

/**
 * Consumer class
 * 
 * Thread reads elements from buffer periodically or sleep if buffer is empty.
 * 
 * @author Gruppe 6: Schmid, Stricker, Laqua
 * 
 */
public class Consumer implements Runnable {

	private Buffer buffer;
	private int sleepTime;

	private String name;

	/**
	 * Creates a new Consumer
	 * 
	 * @param buffer
	 *            the buffer to read from
	 * @param sleepTime
	 *            time to sleep if buffer is empty
	 */
	public Consumer(Buffer buffer, int sleepTime) {

		this.buffer = buffer;
		this.sleepTime = sleepTime;

	}

	@Override
	public void run() {

		int number = -1;
		name = Thread.currentThread().getName();

		do {

			if (buffer.isBufferEmpty()) {

				// print full buffer msg
				System.out.println("\t\t\t\t\t\t" + name
						+ ":\tEmpty Buffer. Sleep for " + sleepTime
						+ " milliseconds.");

				try {
					Thread.sleep(sleepTime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			} else {

				number = buffer.getNextElement();

				System.out.println("\t\t\t\t\t\t" + name + ":\tRead " + number);

			}

		} while (number != 0);

		System.out
				.println("\t\t\t\t\t\t" + name + ":\tRead Zero. Terminating.");

	}
}
