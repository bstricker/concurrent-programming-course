package main;

import java.util.Random;

/**
 * 
 * Producer class
 * 
 * Thread writes elements into buffer periodically and sleeps if buffer is
 * already full.
 * 
 * @author Gruppe 6: Schmid, Stricker, Laqua
 * 
 */
public class Producer implements Runnable {

	private Buffer buffer;
	private int minSleepTime;
	private int maxSleepTime;
	private int fullBufferSleepTime;

	private String name;

	/**
	 * Creates a new Producer
	 * 
	 * @param buffer
	 *            the buffer to write into
	 * @param minSleepTime
	 *            minimal time to sleep after writing into buffer
	 * @param maxSleepTime
	 *            maximal time to sleep after writing into buffer
	 * @param fullBufferSleepTime
	 *            time to sleep if buffer is full
	 */
	public Producer(Buffer buffer, int minSleepTime, int maxSleepTime,
			int fullBufferSleepTime) {

		this.buffer = buffer;
		this.minSleepTime = minSleepTime;
		this.maxSleepTime = maxSleepTime;
		this.fullBufferSleepTime = fullBufferSleepTime;
	}

	@Override
	public void run() {

		Random generator = new Random();
		int number = -1;
		int sleepTime = 0;

		name = Thread.currentThread().getName();

		do {

			if (!buffer.isBufferFull()) {

				// Random number from 0 to 100
				number = generator.nextInt(101);

				System.out.println(name + ": Generated " + number);

				buffer.addElement(number);

				sleepTime = generator.nextInt(maxSleepTime - minSleepTime);
				sleepTime += minSleepTime;

				try {
					Thread.sleep(sleepTime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			} else {

				System.out.println(name + ": Full Buffer. Sleep for "
						+ fullBufferSleepTime + " milliseconds.");

				try {
					Thread.sleep(fullBufferSleepTime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}

		} while (number != 0);

		System.out.println(name + ": Generated Zero. Terminating.");

	}
}
