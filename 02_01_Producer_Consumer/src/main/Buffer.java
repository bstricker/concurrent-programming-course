package main;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Multiple-Element-Buffer
 * 
 * Buffer class which can store a fixed number of Integer value.
 * 
 * Provide Thread-Safe methods for reading (get first Element) and writing the buffer (add Element to the End).
 * 
 * @author Gruppe 6: Schmid, Stricker, Laqua
 * 
 */
public class Buffer {

	private int capacity;
	private Queue<Integer> buffer;

	public Buffer(int capacity) {
		this.capacity = capacity;
		buffer = new LinkedBlockingQueue<Integer>(capacity);
	}

	/**
	 * @return if buffer is empty
	 */
	public boolean isBufferEmpty() {
		return buffer.isEmpty();
	}

	/**
	 * @return if buffer is empty
	 */
	public boolean isBufferFull() {
		return buffer.size() >= this.capacity;
	}

	/**
	 * Return first element from buffer. If the buffer is empty
	 * <code>null</code> will be returned.
	 * 
	 * @return the element
	 */
	public synchronized int getNextElement() {

		return buffer.poll();

	}

	/**
	 * 
	 * If the buffer is empty, the specified Integer will stored. Else nothing
	 * happens.
	 * 
	 * @param element
	 *            the element to add
	 */
	public synchronized void addElement(int element) {

		buffer.offer(element);

	}
}
