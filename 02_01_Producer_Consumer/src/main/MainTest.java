package main;

/**
 * Main class
 * 
 * Creates Buffer, creates and starts Producer and Consumer and waits until
 * everybody has terminated.
 * 
 * @author Gruppe 6: Schmid, Stricker, Laqua
 * 
 */
public class MainTest {



	public static void main(String[] args) {

		Buffer buffer = new Buffer(10);

		Runnable producer = new Producer(buffer, 0, 3000, 1000);
		Runnable consumer = new Consumer(buffer, 1000);

		Thread producerThread = new Thread(producer, "Producer");
		Thread consumerThread = new Thread(consumer, "Consumer");

		producerThread.start();
		consumerThread.start();

		try {
			producerThread.join();
			consumerThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out
				.println("Producer and Consumer terminated. Main class terminating.");

	}

}
