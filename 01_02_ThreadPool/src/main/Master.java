package main;

import java.math.BigDecimal;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Master thread for calculating Pi according to the <a
 * href="http://de.wikipedia.org/wiki/Leibniz-Reihe"> Leibniz formula for
 * pi</a>.
 * <p/>
 * User has to specify a number of Worker threads and the desired accuracy ( +/-
 * deviation from Math.Pi).
 *
 * @author Gruppe 6: Schmid, Stricker, Laqua
 */
public class Master {

    private static int numWorker;
    private static BigDecimal accuracy;
    private static int fracturesPerThread;

    private static BigDecimal MathPi = new BigDecimal(Math.PI);
    private static BigDecimal lowerBound;
    private static BigDecimal upperBound;

    public static void main(String[] args) {

        try {
            numWorker = Integer.parseInt(args[0]);
            accuracy = new BigDecimal(args[1]);
            fracturesPerThread = Integer.parseInt(args[2]);
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e1) {

            System.err
                    .println("Falsche Uebergabeparameter:\n\tMaster numWorker accuracy fracturesPerThread");
            System.err.println("Bsp.:\tMaster 4 0.0001 10");

            return;
        }


        lowerBound = MathPi.subtract(accuracy);
        upperBound = MathPi.add(accuracy);

        Calculator calc = new Calculator();

        System.out.println("Worker: " + numWorker);
        System.out.println("Accuracy: +/- " + accuracy);
        System.out.println("Fractures Per Thread: " +
                fracturesPerThread);
        System.out.println("Expecting Pi between " + lowerBound.doubleValue()
                + " and " + upperBound.doubleValue());

		/*
         * ThreadPoolExecutor verwaltet einen Pool von Worker, in den mit
		 * execute(Worker) Worker hinzugef�gt werden k�nnen.
		 * 
		 * Vor jedem Hinzuf�gen wird �berpr�ft ob noch Threads gestartet werden
		 * k�nnen um Queueing zu vermeiden (w�rde nach Erreichen der Genauigkeit
		 * Queue fertig abarbeiten => unn�tige Arbeit)
		 */
        ThreadPoolExecutor executor = new ThreadPoolExecutor(numWorker,
                numWorker, 60L, TimeUnit.SECONDS,
                new LinkedBlockingDeque<Runnable>());

		/*
         * es werden solange in jedem Durchgang neue Worker erzeugt und
		 * gestartet, bis die Genauigkeit erreicht ist
		 * 
		 * jeder Worker berechnet die FRACS_PER_THREAD Teilsummen von Beginn bis
		 * Ende
		 */

        int begin = 0;
        int end = fracturesPerThread - 1;

        long timeStart = 0;
        long timeEnd = 0;

        timeStart = System.currentTimeMillis();

        while (!checkAccuracy(calc.getPi())) {

            if (executor.getActiveCount() < numWorker) {

                Runnable run = new Worker(calc, begin, end);

                executor.execute(run);
                begin += fracturesPerThread;
                end += fracturesPerThread;
            }

        }

        timeEnd = System.currentTimeMillis();

        executor.shutdown();

        System.out.println("Finished! Result: " + calc.getPi());
        System.out.println("Math.Pi Constant: " + Math.PI);
        System.out.println("Difference: "
                + calc.getPi().subtract(MathPi).abs().doubleValue());
        System.out.println("Elapsed time: " + (double) (timeEnd - timeStart)
                / 1000 + " seconds");

		/*
         * wartet bis eventuell noch rechnende Worker sich beendet haben
		 * 
		 * passiert wenn viele Threads im Pool rechnen und einer letzte
		 * Teilsumme zum Ergebnis addiert => restlichen Worker rechnen trotzdem
		 * weiter
		 */
        try {
            executor.awaitTermination(60, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /**
     * Return true if the passed number is between lower and upper bound.
     *
     * @param num Number to check
     * @return true if num is accurate enough, false otherwise
     */
    private static boolean checkAccuracy(BigDecimal num) {

        return num.compareTo(lowerBound) >= 0 && num.compareTo(upperBound) <= 0;

    }

}
