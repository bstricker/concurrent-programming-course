package main;
import java.math.BigDecimal;

/**
 * Class that is responsible for storing Pi.
 * 
 * @author Gruppe 6: Schmid, Stricker, Laqua
 *
 */
public class Calculator {

	private BigDecimal result = BigDecimal.ZERO;

	public synchronized BigDecimal getPi4() {
		return result;
	}

	public synchronized void addToResult(BigDecimal summand) {
		result = result.add(summand);
	}

	public synchronized BigDecimal getPi() {
		return result.multiply(new BigDecimal(4));
	}

}
