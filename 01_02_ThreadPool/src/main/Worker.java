package main;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Worker thread who calculates a part of the Sum of the <a
 * href="http://de.wikipedia.org/wiki/Leibniz-Reihe"> Leibniz formula for
 * pi</a>.
 * 
 * 
 * After adding the partial sums the result will be passed to the Calculator.
 * 
 * 
 * @author Gruppe 6: Schmid, Stricker, Laqua
 * 
 */
public class Worker implements Runnable {

	private int beginn;
	private int end;
	private Calculator calc;

	private BigDecimal sum = BigDecimal.ZERO;

	public Worker(Calculator calc, int beginn, int end) {

		this.beginn = beginn;
		this.end = end;
		this.calc = calc;
	}

	@Override
	public void run() {

		// String name = Thread.currentThread().getName();
		// System.out.println(name + " started");

		BigDecimal dividend;
		BigDecimal divisor;

		for (int n = beginn; n <= end; n++) {

			// (-1)^n
			dividend = new BigDecimal(Math.pow(-1, n));

			// 2n + 1
			divisor = new BigDecimal(2 * n + 1);

			sum = sum.add(dividend.divide(divisor, 20, RoundingMode.HALF_UP));

		}

		// System.out.println(name + " finished");

		calc.addToResult(sum);

	}
}
