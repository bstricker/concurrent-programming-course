package test;

import main.Master;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;



@RunWith(value = Parameterized.class)
public class TimeTests {

    private final static int PARAMETER = 20;

	private static String accuracy = "0.0000001";

	private int numWorker;

	public TimeTests(int numWorker) {
		this.numWorker = numWorker;
	}

	@Parameterized.Parameters
	public static List<Object[]> data() {
		Object[][] data = new Object[PARAMETER][1];

		for (int i = 1; i <= PARAMETER; i++) {
			data[i - 1][0] = i;
		}

		return Arrays.asList(data);
	}

	@Test
	public void test_numWorker() {

		System.out.println("Threads: " + numWorker);

		for (int i = 1; i <= 5; i++) {
			System.out.println("Run " + i);
			Master.main(new String[]{Integer.toString(numWorker), accuracy, String.valueOf(10)});
		}
	}

    @Test
    public void test_numFracturesPerThread() {

        System.out.println("Fractures per thread: " + numWorker);

        for (int i = 1; i <= 2; i++) {
            System.out.println("Run " + i);
            Master.main(new String[]{Integer.toString(4), accuracy, String.valueOf(numWorker)});
        }
    }


}
