
import java.util.concurrent.atomic.AtomicInteger;

/**
 * NumberRange
 * <p/>
 * Number range class that does not sufficiently protect its invariants
 *
 * @author Brian Goetz and Tim Peierls
 */

public class MyNumberRange {
    // INVARIANT: lower <= upper
    private final AtomicInteger lower = new AtomicInteger(0);
    private final AtomicInteger upper = new AtomicInteger(0);

    public  void setLower(int i) {
        // Warning -- unsafe check-then-act
        if (i > upper.get())
            throw new IllegalArgumentException("can't set lower to " + i + " > upper " + upper);
        lower.set(i);
        printStates();
    }

    public void setUpper(int i) {
        // Warning -- unsafe check-then-act
        if (i < lower.get())
            throw new IllegalArgumentException("can't set upper to " + i + " < lower " + lower);
        upper.set(i);
        printStates();
    }

    // added Getter, not in original version of Goetz and Peierls
    public AtomicInteger getLower() {
        return lower;
    }

    // added Getter, not in original version of Goetz and Peierls
    public AtomicInteger getUpper() {
        return upper;
    }

    public boolean isInRange(int i) {
        return (i >= lower.get() && i <= upper.get());
    }


    private synchronized void printStates() {
        if (lower.get() > upper.get()) {
            System.err.println("Lower: " + lower + "\tUpper: " + upper);
        }
    }

}

