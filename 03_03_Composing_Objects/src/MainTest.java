import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Tests MyNumberRange.java
 * <p/>
 * Creates in endless loop NumberRangeGenerators which are setting lower and upper bounds.
 * When a race condition (lower > upper) occurs, a message is printed to the Error-Stream.

 * @author Gruppe 6: Schmid, Stricker, Laqua
 */

public class MainTest {

    private static final int MAX_NUM = 10;
    private static final int N_THREADS = 10;


    public static void main(String[] args) throws InterruptedException {

        ThreadPoolExecutor exec = new ThreadPoolExecutor(N_THREADS, N_THREADS, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());

        MyNumberRange numberRange = new MyNumberRange();

        /*
        Adding new Threads to spawn random numbers and wait till some Threads produce a Race Condition (Lower > Upper).
         */
        while (true) {


                exec.execute(new NumberRangeGenerator(numberRange));
                exec.execute(new NumberRangeGenerator(numberRange));
                exec.execute(new NumberRangeGenerator(numberRange));

                exec.execute(new NumberRangeTester(numberRange));


        }


    }


    static class NumberRangeGenerator extends Thread {


        MyNumberRange numberRange;
        Random r = new Random();


        public NumberRangeGenerator(MyNumberRange numberRange) {
            this.numberRange = numberRange;
        }

        public void run() {


            int upper;
            int lower = r.nextInt(MAX_NUM);

            do {

                upper = r.nextInt(MAX_NUM);


            } while (upper < lower);


            // Ignore Exception if lower > upper or upper < lower.
            try {

                    numberRange.setLower(lower);
                    numberRange.setUpper(upper);

            } catch (IllegalArgumentException ignored) {
            }


        }

    }


    static class NumberRangeTester extends Thread {

        MyNumberRange numberRange;
        Random r = new Random();


        public NumberRangeTester(MyNumberRange numberRange) {
            this.numberRange = numberRange;
        }

        public void run() {

            int numberToTest = r.nextInt(MAX_NUM);

            boolean isInRange = numberRange.isInRange(numberToTest);


//            System.out.println(numberToTest + " is " + ((isInRange == true) ? "" : "NOT ") + "in range between " + numberRange.getLower() + " and " + numberRange.getUpper());
        }

    }
}
