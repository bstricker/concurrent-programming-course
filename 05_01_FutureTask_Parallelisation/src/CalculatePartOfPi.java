import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.Callable;

/**
 * Class that is responsible for calculating a partial sum of Pi.
 * <p/>
 * Range will be submitted in the constructor.
 *
 * @author Gruppe 6: Schmid, Stricker, Laqua
 */
public class CalculatePartOfPi implements Callable<BigDecimal> {

    private int start;
    private int end;

    private BigDecimal sum = BigDecimal.ZERO;

    public CalculatePartOfPi(int start, int end) {

        this.start = start;
        this.end = end;


    }

    @Override
    public BigDecimal call() throws Exception {


        BigDecimal dividend;
        BigDecimal divisor;

        for (int n = start; n <= end; n++) {

            // (-1)^n
            dividend = new BigDecimal(Math.pow(-1, n));

            // 2n + 1
            divisor = new BigDecimal(2 * n + 1);

            sum = sum.add(dividend.divide(divisor, 20, RoundingMode.HALF_UP));

        }

//        Thread.sleep(10000);

        return sum;
    }
}
