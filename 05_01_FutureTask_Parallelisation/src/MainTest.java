import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Master thread for calculating Pi according to the <a
 * href="http://de.wikipedia.org/wiki/Leibniz-Reihe"> Leibniz formula for
 * pi</a>.
 * <p/>
 *
 * Start a number of FutureTasks that will calculate a partial sum of Pi.
 *
 * Number of calculators (FutureTasks) can be configured in the CALCULATORS array. Note: All configurations will be executed one after the other!
 *
 * @author Gruppe 6: Schmid, Stricker, Laqua
 */


public class MainTest {

    private static final int[] CALCULATORS = new int[]{1, 2, 4, 80, 160, 320, 1024, 4096};
    private static final int FRACS = 10000;


    public static void main(String[] args) throws InterruptedException {


        for (int calculators : CALCULATORS) {


            ThreadPoolExecutor exec = new ThreadPoolExecutor(calculators, calculators, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());

            List<FutureTask<BigDecimal>> tasks = new LinkedList<FutureTask<BigDecimal>>();

            int begin = 0;
            int end = FRACS - 1;

            long timeBeforeInit;
            long timeAfterInit;
            long timeEnd;

            BigDecimal sum = BigDecimal.ZERO;

            System.out.println("Calculate Pi with " + calculators + " parallel FutureTasks...");

            timeBeforeInit = System.nanoTime();

            for (int i = 0; i < calculators; i++) {

                CalculatePartOfPi calculator = new CalculatePartOfPi(begin, end);
                FutureTask<BigDecimal> futureTask = new FutureTask<BigDecimal>(calculator);

                tasks.add(futureTask);

                exec.execute(futureTask);

                begin += FRACS;
                end += FRACS;
            }

            timeAfterInit = System.nanoTime();
            System.out.println("FutureTask created");

            for (FutureTask<BigDecimal> task : tasks) {
                try {
                    sum = sum.add(task.get());
                } catch (InterruptedException e) {
                    System.err.println("One Thread interrupted! Result may not be correct.");
                } catch (ExecutionException e) {
                    System.err.println("One Thread died! Result may not be correct.");
                    e.printStackTrace();
                }
            }


            sum = sum.multiply(new BigDecimal(4));

            timeEnd = System.nanoTime();

            exec.shutdown();
            exec.awaitTermination(5, TimeUnit.SECONDS);


            System.out.println("Result ready");
            System.out.println(sum);

            System.out.println("Error: " + (new BigDecimal(Math.PI).subtract(sum)));

            System.out.println("Elapsed time (with FutureTask initialization)\t" + (timeEnd - timeBeforeInit) / Math.pow(10, 9));
            System.out.println("Elapsed time (without FutureTask initialization)\t" + (timeEnd - timeAfterInit) / Math.pow(10, 9));

            System.out.println("\n----------------------------------------------\n");

        }

    }

}
