import java.text.DecimalFormat;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * Main Class
 * <p/>
 * Create a AtomicIntegerArray on which different Threads (Incrementer and Decrementer) increment/decrement random values by a random value.
 * Measure the time how long these contradictory task need to get all values > 30.
 * <p/>
 * Run this Measurement very often and compute minimal, maximal and average time.
 */
public class Main {

    private static final int NUM_RUNS = 50;
    private static final int NUM_TASK = 8;

    // Display result with two decimals
    private static final DecimalFormat df = new DecimalFormat("#.##");


    private static double sMinTime;
    private static double sMaxTime;
    private static double sAvgTime;

    private static int sMinRun;
    private static int sMaxRun;


    public static void main(String[] args) throws InterruptedException {


        double[] execTimes = new double[NUM_RUNS];


        for (int run = 0; run < NUM_RUNS; run++) {

            final AtomicIntegerArray array = new AtomicIntegerArray(100);


            long startTime;
            long endTime;
            double execTimeInMS;

            ExecutorService executorService = Executors.newFixedThreadPool(NUM_TASK);


            Thread checkTask = new Thread(new CheckTask(array));

            startTime = System.nanoTime();

            checkTask.start();

            while (checkTask.isAlive()) {


                // Incrementer
                for (int i = 0; i < NUM_TASK / 2; i++) {
                    FutureTask<Void> futureTask = new FutureTask<Void>(new IncrementTask(array), null);
                    executorService.execute(futureTask);
                }

                // Decrementer
                for (int i = 0; i < NUM_TASK; i++) {
                    FutureTask<Boolean> futureTask = new FutureTask<Boolean>(new DecrementTask(array));
                    executorService.execute(futureTask);
                }

                FutureTask<Void> incTask = new FutureTask<Void>(new IncrementTask(array), null);
                FutureTask<Boolean> decTask = new FutureTask<Boolean>(new DecrementTask(array));

                incTask.run();
                decTask.run();


            }

            endTime = System.nanoTime();

            executorService.shutdownNow();

            // Execution time in mili seconds
            execTimeInMS = (endTime - startTime) / Math.pow(10, 6);

            execTimes[run] = execTimeInMS;

//            System.out.println(array);

            System.out.println("Execution time run " + (run + 1) + ": " + df.format(execTimeInMS) + " ms\n");

        }

        calcTimes(execTimes);

        System.out.println("Minimal execution time: " + df.format(sMinTime) + "\tRun " + sMinRun);
        System.out.println("Maximal execution time: " + df.format(sMaxTime) + "\tRun " + sMaxRun);
        System.out.println("Average execution time: " + df.format(sAvgTime));
    }

    /**
     * Calculate min, max and avg time based on the specified time array.
     * <p/>
     * Stores calculated times in the static variables sMinTime, sMaxTime, sAvgTime.
     *
     * @param execTimes times to calculate the min, max and avg time.
     */
    private static void calcTimes(double[] execTimes) {

        double minTime = execTimes[0];
        double maxTime = 0.0;
        int minRun = 0;
        int maxRun = 0;

        double sumTime = 0.0;

        for (int i = 0; i < execTimes.length; i++) {

            double time = execTimes[i];

            if (time < minTime) {
                minRun = i;
                minTime = time;
            }

            if (time > maxTime) {
                maxRun = i;
                maxTime = time;
            }

            sumTime += time;

        }

        sAvgTime = sumTime / execTimes.length;
        sMinTime = minTime;
        sMaxTime = maxTime;

        sMinRun = minRun;
        sMaxRun = maxRun;

    }
}
