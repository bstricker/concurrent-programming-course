import java.util.Random;
import java.util.concurrent.atomic.AtomicIntegerArray;


/**
 * Task that increments a random slot of the specified AtomicIntegerArray by a random value [1-10] if the original value is not greater than 30.
 */
public class IncrementTask implements Runnable {

    private final AtomicIntegerArray array;
    private final Random random;

    public IncrementTask(AtomicIntegerArray array) {

        this.array = array;
        this.random = new Random();

    }

    @Override
    public void run() {

        int slot = random.nextInt(array.length());
        int currValue = array.get(slot);

        if (currValue < 30) {

            int inc = random.nextInt(10) + 1;
            int newValue = currValue + inc;

            array.compareAndSet(slot, currValue, newValue);
        }
    }
}
