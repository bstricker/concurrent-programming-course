import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * Thread that checks periodically if in the AtomicIntegerArray every value is > 30
 */
public class CheckTask implements Runnable {

    private final AtomicIntegerArray array;

    public CheckTask(AtomicIntegerArray array) {

        this.array = array;

    }


    @Override
    public void run() {

        boolean allGreater30;

        do {

            // assume every check cycle all values are greater 30
            allGreater30 = true;

            for (int i = 0; i < array.length(); i++) {

                int value = array.get(i);
                // found one value not greater, set flag to false
                if (value < 30) {
                    allGreater30 = false;

                    System.out.println(value + " smaller 30 (" + i + ")");

                    // sleep and check later again
                    try {
                        Thread.sleep(2);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    //  check from the beginning again
                    i = 0;

                }

            }
        } while (!allGreater30);


    }
}
