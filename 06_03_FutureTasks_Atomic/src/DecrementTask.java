import java.lang.Boolean;import java.lang.Exception;import java.lang.Integer;import java.lang.Override;import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * Task that decrements a random slot of the specified AtomicIntegerArray by a random value [1-3] if the original value is not greater than 30.
 */
public class DecrementTask implements Callable<Boolean> {

    private final AtomicIntegerArray array;
    private final Random random;

    public DecrementTask(AtomicIntegerArray array) {

        this.array = array;
        this.random = new Random();

    }

    @Override
    public Boolean call() throws Exception {

        int slot = random.nextInt(array.length());
        int currValue = array.get(slot);

        if (currValue < 30 && currValue > Integer.MIN_VALUE) {

            int dec = random.nextInt(3) + 1;
            int newValue = currValue - dec;

            array.compareAndSet(slot, currValue, newValue);
        }
        return true;
    }
}
